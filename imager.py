import re
import os
import subprocess

lang = os.getenv("LANG")
repo = os.getenv("REPO")
dloggin = os.getenv("DLOGGIN")
dpass = os.getenv("DPASS")
pom = "<version>"
find = False


def java():
    with open('files/pom.xml') as fin:
        for line in fin:
            global find
            if pom in line and not find:
                print("Existe en " + line)
                find = True
                result = re.search('version>(.*)</version>', line)
                version = (result.group(1))

    invoke(repo + ":" + version)


def node():
    print("Esto es node")
    with open('files/package.json') as fin:
        for line in fin:
            if '\"version\"' in line and not find:
                print("Existe en " + line)
                version = line.split("\"")[3]
                print(line.split("\"")[3])

    invoke(repo + ":" + version)


def invoke(rep):
    subprocess.run(["docker", "login", "--username=" + dloggin, "--password=" + dpass])
    subprocess.run(["docker", "build", "-t", rep, "-f", "files/Dockerfile", "."])
    subprocess.run(["docker", "push", rep])


try:
    exec(lang + "()")
except:
    print("LA OPCION ELEGIDA NO EXISTE")
