## DESCRIPCION:
Este contenedor se ha diseñado para subir imagenes al registro de docker con el tag que haya en el pom en el caso de java.  
*Este contenedor usa docker in docker.*

Requisitos (variables de entorno):  
**LANG=** (Ej: java)  
**REPO=** (Ej: miglesiassarria/mi-repo)  
**DLOGGIN=** (Usuario de cuenta docker Hub)  
**DPASS=** (Password de la cuenta)  

MODO DE EMPLEO:  
Es necesario montar el proyecto en  /code/files. Una ejecución quedaria en este estilo:  


Lenguajes soprtados:
- java
- node

```
docker run --rm -v $PWD:/code/files -v /var/run/docker.sock:/var/run/docker.sock -e LANG=java -e REPO=user/repo -e DLOGGIN=user -e DPASS=pass miglesiassarria/ver2dochub
```

